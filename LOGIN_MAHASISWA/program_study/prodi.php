<?php

include '../../config/config.php';

session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../LOGIN_MAHASISWA/index.php");
}
?>

<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8" />
    <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
    <link rel="stylesheet" href="style.css" />
    <!-- Boxicons CDN Link -->
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="sidebar">
      <div class="logo-details">
        <i class="bx bx-notepad"></i>
        <span class="logo_name">Attendance Report</span>
      </div>
      <ul class="nav-links">
        <li>
          <a href="../index.php">
            <i class="bx bx-grid-alt"></i>
            <span class="links_name">Beranda</span>
          </a>
        </li>
        <li>
          <a href="#" class="active">
            <i class="bx bx-list-ul"></i>
            <span class="links_name">Mata Kuliah</span>
          </a>
        </li>
        <li>
          <a href="../absent/index.php">
            <i class="bx bx-food-menu"></i>
            <span class="links_name">Rekap</span>
          </a>
        </li>
        <li class="log_out">
          <a href="../../login/index.php">
            <i class="bx bx-log-out"></i>
            <span class="links_name">Keluar</span>
          </a>
        </li>
      </ul>
    </div>
    <section class="home-section">
      <nav>
        <div class="sidebar-button">
          <i class="bx bx-menu sidebarBtn"></i>
          <span class="dashboard">Mata Kuliah</span>
        </div>
      </nav>

      <div class="home-content">
        <div class="overview-boxes">
          <div class="box">
            <div class="right-side">
              <div class="box-topic">Mata Kuliah</div>
              <div class="number">TRPL</div>
              <div class="indicator">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style="fill: rgba(12, 25, 207, 1);transform: msFilter"><path d="M12 2C6.486 2 2 6.486 2 12s4.486 10 10 10 10-4.486 10-10S17.514 2 12 2zm0 15-5-5h4V7h2v5h4l-5 5z"></path></svg>
                <span class="text">Absensi TRPL</span>
              </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" style="fill: rgba(12, 25, 207, 1);transform: msFilter"><path d="M2 7v1l11 4 9-4V7L11 4z"></path><path d="M4 11v4.267c0 1.621 4.001 3.893 9 3.734 4-.126 6.586-1.972 7-3.467.024-.089.037-.178.037-.268V11L13 14l-5-1.667v3.213l-1-.364V12l-3-1z"></path></svg>
          </div>
        </div>

        <!-- ini containernya  -->

        <!-- matematika Diskrit -->
        <div class="sales-boxes" style="padding: 10px;">

        <?php 
              $sql    = "SELECT * FROM mata_kuliah WHERE id_matkul <= 119 ";
              $query  = mysqli_query($con, $sql);
              while ($row = mysqli_fetch_array($query)) {
                $id = $row['id_matkul'];
                $nama_satu = $row['nama_matkul'];
                $dosen = $row['nama_dosen']; 
              ?>         
          <div class="top-sales box">
            
            <div class="title"><?php echo $nama_satu?></div>
            <ul class="top-sales-details">
              <li>
                  <!--<img src="images/addidas.jpg" alt="">-->
                  <span class="product"><?php echo $dosen?></span>
              </li>
              <br><br>     
              <div class="button">            
                <a href="../program_study/index3.php?mt=<?= $id ?>&nm=<?= $nama_satu?>&nd=<?= $dosen?>">Absen Disini</a>
              
              </div>
            </ul>
          </div>
          <?php 
        }?>   
        </div>

         <!-- ini containernya yang kedua  -->

        <!-- matematika Diskrit -->
        <div class="sales-boxes">

        <?php 
              $sql    = "SELECT * FROM mata_kuliah WHERE id_matkul >= 121 AND id_matkul <=123 ";
              $query  = mysqli_query($con, $sql);
          
              while ($row = mysqli_fetch_array($query)) {
                $id = $row['id_matkul'];
                $nama_satu = $row['nama_matkul'];
                $dosen = $row['nama_dosen']; 
              ?>         
          <div class="top-sales box">
            
            <div class="title"><?php echo $nama_satu?></div>
            <ul class="top-sales-details">
              <li>
                  <!--<img src="images/addidas.jpg" alt="">-->
                  <span class="product"><?php echo $dosen?></span>
              </li>
              <br><br>     
              <div class="button">            
                <a href="../program_study/index3.php?mt=<?= $id ?>&nm=<?= $nama_satu?>&nd=<?= $dosen?>">Absen Disini</a>
              
              </div>
            </ul>
          </div>
          <?php 
        }?>   
        </div>

         <!-- ini containernya ini yang ketiga  -->

        <div class="sales-boxes" style="padding: 10px;">

        <?php 
              $sql    = "SELECT * FROM mata_kuliah WHERE id_matkul > 123";
              $query  = mysqli_query($con, $sql);
          
              while ($row = mysqli_fetch_array($query)) {
                $id = $row['id_matkul'];
                $nama_satu = $row['nama_matkul'];
                $dosen = $row['nama_dosen']; 
              ?>         
          <div class="top-sales box">
            
            <div class="title"><?php echo $nama_satu?></div>
            <ul class="top-sales-details">
              <li>
                  <!--<img src="images/addidas.jpg" alt="">-->
                  <span class="product"><?php echo $dosen?></span>
              </li>
              <br><br>     
              <div class="button">            
                <a href="../program_study/index3.php?mt=<?= $id ?>&nm=<?= $nama_satu?>&nd=<?= $dosen?>">Absen Disini</a>
              
              </div>
            </ul>
          </div>
          <?php 
        }?>   
        
        </div>





       

      </div>
    </section>

    <script>
      let sidebar = document.querySelector('.sidebar');
      let sidebarBtn = document.querySelector('.sidebarBtn');
      sidebarBtn.onclick = function () {
        sidebar.classList.toggle('active');
        if (sidebar.classList.contains('active')) {
          sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
        } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
      };
    </script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>