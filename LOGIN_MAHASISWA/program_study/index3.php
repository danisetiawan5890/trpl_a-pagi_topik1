<?php

include '../../config/config.php';
date_default_timezone_set('asia/jakarta');
session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../LOGIN_MAHASISWA/index.php");
}

$id_matkul = $_GET['mt'] ?? NULL;
$nama_matkul = $_GET['nm'] ?? NULL;
$dosen_matkul = $_GET['nd'] ?? NULL;






if(isset($_POST['submit'])) {


  $tgl = date('Y-m-d');

  if($_GET['a'] == 'M'){
    $nama = $_SESSION['username'];
    $w_masuk = new DateTime("08:00");
    $now = new DateTime();
    $masuk = date("H:i");
    $ket = $_POST['absen'];
    // $masuk = "08:00";
    $j = $now->diff($w_masuk)->h;
    $m = $now->diff($w_masuk)->i;
    if($now < $masuk){
      $late = "0.0";
    }else{
      $late = $j.'.'.$m;
    }
  }

  $kode_matakuliah = $_POST['id_matkul'];
  $nama_matkul = $_POST['nama_matkul'];
  $query = mysqli_query($con,"INSERT INTO tb_absensi(matkul,nama_matkul,nim,masuk,terlambat,tgl_absen,keterangan) VALUES('$kode_matakuliah','$nama_matkul','$nama','$masuk','$late','$tgl','$ket')");
  if($query) {
    header('location: ../program_study/index3.php?mt=' .$kode_matakuliah);
  }
}
?>


<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8" />
    <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
    <link rel="stylesheet" href="style.css" />
    <!-- Boxicons CDN Link -->
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <div class="sidebar">
      <div class="logo-details">
        <i class="bx bx-notepad"></i>
        <span class="logo_name">Attendance Report</span>
      </div>
      <ul class="nav-links">
        <li>
          <a href="../index.php">
            <i class="bx bx-grid-alt"></i>
            <span class="links_name">Beranda</span>
          </a>
        </li>
        <li>
          <a href="../program_study/prodi.php" class="active">
            <i class="bx bx-list-ul"></i>
            <span class="links_name">Mata Kuliah</span>
          </a>
        </li>
        <li>
          <a href="../absent/index.php">
            <i class="bx bx-food-menu"></i>
            <span class="links_name">Rekap</span>
          </a>
        </li>
        <li class="log_out">
          <a href="../../login/index.php">
            <i class="bx bx-log-out"></i>
            <span class="links_name">Keluar</span>
          </a>
        </li>
      </ul>
    </div>
    <section class="home-section">
      <nav>
        <div class="sidebar-button">
          <i class="bx bx-menu sidebarBtn"></i>
          <span class="dashboard"><a style="text-decoration: none; color: black;" href="../program_study/prodi.php"> Mata Kuliah </a>> <?= $nama_matkul?></span>
        </div>
      </nav>

      <div class="home-content">
        <div class="sales-boxes">        
          <div class="recent-sales box">
            <div class="title"><?= $nama_matkul?></div>
            <span class="product"><?= $dosen_matkul?></span>
            <br><br>
            <p>Keterangan Absensi Hari ini!</p>

            <!-- program membuat batas absensinya -->
      <?php 
        $user = $_SESSION['username'];
        $tgl = date('Y-m-d');
        $query = mysqli_query($con, "SELECT * FROM tb_absensi WHERE nim = '$user' AND tgl_absen = '$tgl' AND matkul = '$id_matkul'");
        $data = mysqli_fetch_array($query);
        $cek = mysqli_num_rows($query);
        $now = strtotime (date('H:i') );

        // $now = "21:20";
        if($cek != 1) {

          $set_buka = strtotime("08:00");
          $set_tutup = strtotime("23:00");
          // $jam_masuk = "22:01"
          if($now < $set_buka) {
            echo "absen masuk dibuka 10:00";
          }
          else if($now <= $set_tutup) {
           
        ?>
            <form action="index3.php?a=M" method="POST">
            <input hidden name="id_matkul" value="<?= $id_matkul ?>" />
            <input hidden name="nama_matkul" value="<?= $nama_matkul?>" />
              
              <input type="radio" name="absen" required id="hadir" value="hadir">
              <label for="hadir">hadir</label><br>
              <input type="radio" name="absen" required id="sakit" value="sakit">
              <label for="sakit">sakit</label><br>
              <input type="radio" name="absen" required id="izin" value="izin">
              <label for="izin">izin</label><br>
              <input type="radio" name="absen" required id="alfa" value="alfa">
              <label for="alfa">alfa</label><br>
              <tr>
                <td>
                  <br>
                  <p>absen ditutup pada 14:00</p>
                  <button style=" color: #fff;
                  background: #0a2558;
                  padding: 6px 12px;
                  font-size: 15px;
                  font-weight: 400;
                  border-radius: 4px;
                  text-decoration: none;
                  transition: all 0.3s ease;" type="submit" name="submit">kirimkan</button>
                  </form>
                  <?php }?>
            
            <?php }else if($cek == 1) {
            if($data['masuk'] != null) {
              echo "<br>absen hari ini telah direkam";  
            ?>
          <?php }?>
          <?php }?>
                </td>
              </tr>	
            </div>
          </div>
        

          
          </div>

        </div>
        
       

      </div>
    </section>

    <script>
      let sidebar = document.querySelector('.sidebar');
      let sidebarBtn = document.querySelector('.sidebarBtn');
      sidebarBtn.onclick = function () {
        sidebar.classList.toggle('active');
        if (sidebar.classList.contains('active')) {
          sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
        } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
      };
    </script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>