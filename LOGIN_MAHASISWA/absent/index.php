<?php

include '../../config/config.php';
date_default_timezone_set('asia/jakarta');

session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../LOGIN_MAHASISWA/index.php");
}

?>

<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8" />
    <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
    <link rel="stylesheet" href="style.css" />
    <!-- Boxicons CDN Link -->
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- datepicker nya -->
     <!-- Custom styles for this template-->
     <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../vendor/datepicker/datepicker3.css">

  </head>
  <body>
    <div class="sidebar">
      <div class="logo-details">
        <i class="bx bx-notepad"></i>
        <span class="logo_name">Attendance Report</span>
      </div>
      <ul class="nav-links">
        <li>
          <a href="../index.php">
            <i class="bx bx-grid-alt"></i>
            <span class="links_name">Beranda</span>
          </a>
        </li>
        <li>
          <a href="../program_study/prodi.php">
            <i class="bx bx-list-ul"></i>
            <span class="links_name">Mata Kuliah</span>
          </a>
        </li>
        <li>
          <a href="../absent/index.php" class="active">
            <i class="bx bx-food-menu"></i>
            <span class="links_name">Rekap</span>
          </a>
        </li>
        <li class="log_out">
          <a href="../../login/index.php">
            <i class="bx bx-log-out"></i>
            <span class="links_name">Keluar</span>
          </a>
        </li>
      </ul>
    </div>
    <section class="home-section">
      <nav>
        <div class="sidebar-button">
          <i class="bx bx-menu sidebarBtn"></i>
          <span class="dashboard">Rekap</span>
        </div>
      </nav>


      <div class="home-content">
        <div class="sales-boxes">
          <div class="recent-sales box">
            <div class="activity">
            <div class="card-header">
            <h5 class="m-0 font-weight-bold text-primary text-center">Data Absensi</h5>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Kode Matkul</th>
                        <th>Mata kuliah</th>
                        <th>Nim</th>
                        <th>Masuk</th>
                        <th>Terlambat</th>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = $_SESSION['username'];
                        $query = mysqli_query($con, "SELECT * FROM tb_absensi WHERE nim = '$i'");
                        $no = 1;
                        while ($data = mysqli_fetch_array($query)) {
                        ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data['matkul'] ?></td>
                        <td><?= $data['nama_matkul'] ?></td>
                        <td><?= $data['nim'] ?></td>
                        <td><?= $data['masuk'] ?></td>
                        <td><?= $data['terlambat'] ?></td>
                        <td><?= $data['tgl_absen'] ?></td>
                        <td><?= $data['keterangan'] ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
            </div>
              </div>
            </div>
          </div>
    </section>

    <script>
      let sidebar = document.querySelector('.sidebar');
      let sidebarBtn = document.querySelector('.sidebarBtn');
      sidebarBtn.onclick = function () {
        sidebar.classList.toggle('active');
        if (sidebar.classList.contains('active')) {
          sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
        } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
      };
    </script>
  </body>
</html>