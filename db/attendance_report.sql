-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Des 2022 pada 14.54
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendance_report`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_kuliah`
--

CREATE TABLE `mata_kuliah` (
  `id_matkul` int(15) NOT NULL,
  `kode_matkul` varchar(30) NOT NULL,
  `id_user` varchar(15) NOT NULL,
  `nama_matkul` varchar(60) NOT NULL,
  `nama_dosen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `mata_kuliah`
--

INSERT INTO `mata_kuliah` (`id_matkul`, `kode_matkul`, `id_user`, `nama_matkul`, `nama_dosen`) VALUES
(112, 'MK001', 'NIP-001', 'Matematika Diskrit', 'Siskha Handayani, M.Si'),
(113, 'MK002', 'NIP-002', 'Pengantar Basis Data', 'Ahmadi Irmansyah Lubis, S.Kom., M.Kom.'),
(119, 'MK003', 'NIP-003', 'Pemograman Berbasis Web', 'Noper Ardi, S.Pd., M.Eng'),
(121, 'MK004', 'NIP-004', 'Algoritma dan Pemograman', 'Hamdani Arif, S.Pd.,M.Sc'),
(122, 'MK005', 'NIP-005', 'Pengantar Rekayasa Perangkat Lunak', 'Metta Santiputri, ST, M.Sc, Ph.D'),
(123, 'MK006', 'NIP-006', 'Analisis Dan Spesifikasi Kebutuhan Perangkat Lunak', 'Supardianto, M.Eng.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_absensi`
--

CREATE TABLE `tb_absensi` (
  `id_absen` int(15) NOT NULL,
  `matkul` varchar(45) NOT NULL,
  `nama_matkul` varchar(60) NOT NULL,
  `nim` varchar(25) NOT NULL,
  `masuk` time NOT NULL,
  `terlambat` decimal(6,2) NOT NULL,
  `tgl_absen` date NOT NULL,
  `keterangan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `tb_absensi`
--

INSERT INTO `tb_absensi` (`id_absen`, `matkul`, `nama_matkul`, `nim`, `masuk`, `terlambat`, `tgl_absen`, `keterangan`) VALUES
(96, '119', 'Pemograman Berbasis Web', '4342201004', '10:42:00', '2.42', '2022-12-22', 'hadir'),
(97, '112', 'Matematika Diskrit', '4342201001', '10:50:00', '2.50', '2022-12-22', 'hadir'),
(98, '113', 'Pengantar Basis Data', '4342201001', '10:50:00', '2.50', '2022-12-22', 'hadir'),
(99, '123', 'Analisis Dan Spesifikasi Kebutuhan Perangkat Lunak', '4342201001', '10:56:00', '2.56', '2022-12-22', 'hadir');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  `sebagai` enum('mahasiswa','dosen','staff') NOT NULL,
  `jenis_kelamin` enum('male','female') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `nama`, `prodi`, `password`, `sebagai`, `jenis_kelamin`) VALUES
(43, 'admin', 'admin', 'Teknologi Rekayasa Perangkat Lunak', 'admin', 'staff', 'male'),
(44, 'NIP-001', 'Siskha Handayani, M.Si', 'Matematika Diskrit', 'NIP-001', 'dosen', 'female'),
(49, 'NIP-002', 'Ahmadi Irmansyah Lubis, S.Kom., M.Kom.', 'Pengantar Basis Data', 'NIP-002', 'dosen', 'male'),
(50, 'NIP-003', 'Noper Ardi, S.Pd., M.Eng', 'Pemograman Berbasis Web', 'NIP-003', 'dosen', 'male'),
(51, 'NIP-004', 'Hamdani Arif, S.Pd.,M.Sc', 'Algoritma dan Pemograman', 'NIP-004', 'dosen', 'male'),
(52, 'NIP-005', 'Metta Santiputri, ST, M.Sc, Ph.D', 'Pengantar Rekayasa Perangkat Lunak', 'NIP-005', 'dosen', 'female'),
(53, 'NIP-006', 'Supardianto, M.Eng.', 'Analisis Dan Spesifikasi Kebutuhan Perangkat Lunak', 'NIP-006', 'dosen', 'male'),
(54, '4342201001', 'Seto Cahyadin', 'Teknologi Rekayasa Perangkat Lunak', '4342201001', 'mahasiswa', 'male'),
(55, '4342201002', 'Noval Zahwan Fhalma', 'Teknologi Rekayasa Perangkat Lunak', '4342201002', 'mahasiswa', 'male'),
(56, '4342201003', 'Muhammad Azrul', 'Teknologi Rekayasa Perangkat Lunak', '4342201003', 'mahasiswa', 'male'),
(57, '4342201004', 'Dany Setiawan Maulana Aziz', 'Teknologi Rekayasa Perangkat Lunak', '4342201004', 'mahasiswa', 'male'),
(58, '4342201005', 'Fitriani', 'Teknologi Rekayasa Perangkat Lunak', '4342201005', 'mahasiswa', 'female');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`id_matkul`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `tb_absensi`
--
ALTER TABLE `tb_absensi`
  ADD PRIMARY KEY (`id_absen`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nim` (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  MODIFY `id_matkul` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT untuk tabel `tb_absensi`
--
ALTER TABLE `tb_absensi`
  MODIFY `id_absen` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD CONSTRAINT `mata_kuliah_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
