<?php
session_start();


require '../config/config.php';
mysqli_report(MYSQLI_REPORT_ALL);


if ($_SERVER['REQUEST_METHOD'] == "POST") {

  $username = $_POST['username'];
  $password = $_POST['password'];

  $query = "SELECT * FROM tb_user WHERE username = '$username' limit 1";

  $result = mysqli_query($con, $query);

  if ($result) {
    if ($result && mysqli_num_rows($result) > 0) {
      $user_data = mysqli_fetch_assoc($result);
      if ($user_data['password'] === $password) {
        $_SESSION['nama'] = $user_data['nama'];
        $_SESSION['prodi'] = $user_data['prodi'];
        $_SESSION['sebagai'] = $user_data['sebagai'];
        $_SESSION['username'] = $user_data['username'];  
        $_SESSION['jenis_kelamin'] = $user_data['jenis_kelamin'];
        if ($user_data['sebagai'] == "mahasiswa") {
          $_SESSION['username'] = $username;
          $_SESSION['sebagai'] = "mahasiswa";
          header("Location: ../LOGIN_MAHASISWA/index.php");
        } else if ($user_data['sebagai'] == "dosen") {
          header("Location: ../LOGIN_DOSEN/index.php");
        } else if ($user_data['sebagai'] == "staff") {
          header("Location: ../LOGIN_STAFF/index.php");
        }

        die;
      } else {
          echo '<script type ="text/JavaScript">';  
          echo 'alert("Password atau ID Anda Salah")';  
          echo '</script>';
      }
    }
    
  }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <title>Login Attendance Report</title>

  <style>
    * {
      padding: 0;
      margin: 0;
      box-sizing: border-box;
    }

    body {
      background: rgb(219, 226, 226);
    }

    .row {
      background: white;
      border-radius: 20px;
    }

    img {
      border-top-left-radius: 30px;
      border-bottom-left-radius: 30px;
    }

    .btn1 {
      border: none;
      outline: none;
      height: 50px;
      width: 100%;
      background-color: rgb(21, 83, 218);
      color: whitesmoke;
      border-radius: 4px;
      font-weight: bold;
    }

    .img2 {
      width: 150px;
      height: 140px;
    }

    .h1 {
      font-family: serif;
    }
  </style>
</head>

<body>
  <section class="Form my-4 mx-5">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-lg-5">
          <img src="poltek.jpg" class="img-fluid" alt="" />
        </div>
        <div class="col-lg-7 px-5 pt-5">
          <center>
            <img src="logo Attendance report.png" class="img2" alt="" />
          </center>
          <center>
            <h1 class="font-weight-bold py-3 h1">Attendance Report</h1>
            <h4>Masuk ke akun anda</h4>
            <form action="" method="POST">
              <div class="form-row">
                <div class="col-lg-7">
                  <input type="text" class="form-control my-3 p-4" placeholder="Username " name="username" id="" required />
                </div>
              </div>
              <div class="form-row">
                <div class="col-lg-7">
                  <input type="password" class="form-control my-3 p-4" placeholder="Password" name="password" id="" required />
                </div>
              </div>
              <div class="form-row">
                <div class="col-lg-7"><button type="submit" class="btn1">Masuk</button></div>
              </div>
          </center>
          </form>
        </div>
      </div>
    </div>
  </section>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

  <?php
  session_destroy();
  ?>
</body>

</html>