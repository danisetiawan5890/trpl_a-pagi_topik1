<?php

include '../config/config.php';

session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../LOGIN_STAFF/index.php");
}
?>

<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">

<head>
  <meta charset="UTF-8" />
  <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
  <link rel="stylesheet" href="style.css" />
  <!-- Boxicons CDN Link -->
  <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <div class="sidebar">
    <div class="logo-details">
      <i class="bx bx-notepad"></i>
      <span class="logo_name">Attendance Report</span>
    </div>
    <ul class="nav-links">
      <li>
        <a href="#" class="active">
          <i class="bx bx-grid-alt"></i>
          <span class="links_name">Beranda</span>
        </a>
      </li>
      <li>
        <a href="../LOGIN_STAFF/Tambah/index.php">
          <i class="bx bx-user"></i>
          <span class="links_name">Tambah Pengguna</span>
        </a>
      </li>
      <li>
        <a href="../LOGIN_STAFF/Tambah-matkul/index.php">
        <i class="bx bx-food-menu"></i>
          <span class="links_name">Tambah Mata Kuliah</span>
        </a>
      </li>
      <li class="log_out">
        <a href="../login/index.php">
          <i class="bx bx-log-out"></i>
          <span class="links_name">Keluar</span>
        </a>
      </li>
    </ul>
  </div>
  <section class="home-section">
    <nav>
      <div class="sidebar-button">
        <i class="bx bx-menu sidebarBtn"></i>
        <span class="dashboard">Beranda</span>
      </div>
    </nav>

    <div class="home-content">
      <div class="sales-boxes">
        <div class="recent-sales box">
          <div class="title">Staff</div>
          <div class="sales-details">
            <div class="row">
              <div class="col-md-6">
                <?php echo "<h2> Selamat Datang, " . $_SESSION['nama'] . "" . "</h2>"; ?><br>
                <p>Nama : <?php echo $_SESSION['nama']; ?></p>
                <p>Username : <?php echo $_SESSION['username']; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="top-sales box">
          <div class="title">Teknik Informatika</div>
          <ul class="top-sales-details">
            <li>
              <a href="#">
                <!--<img src="images/sunglasses.jpg" alt="">-->
                <span class="product">Teknologi Rekayasa Perangkat Lunak</span>
              </a>
              <span class="price">D4</span>
            </li>
            <li>
              <a href="#">
                <!--<img src="images/jeans.jpg" alt="">-->
                <span class="product">Rekayasa Keamanan Siber</span>
              </a>
              <span class="price">D4</span>
            </li>
            <li>
              <a href="#">
                <!-- <img src="images/nike.jpg" alt="">-->
                <span class="product">Geomatika</span>
              </a>
              <span class="price">D3</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <script>
    let sidebar = document.querySelector('.sidebar');
    let sidebarBtn = document.querySelector('.sidebarBtn');
    sidebarBtn.onclick = function() {
      sidebar.classList.toggle('active');
      if (sidebar.classList.contains('active')) {
        sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
      } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
    };
  </script>

  <?php if (isset($_GET['status'])) : ?>
    <p>
      <?php
      if ($_GET['status'] == 'sukses') {
        echo "Pendaftaran siswa baru berhasil!";
      } else {
        echo "Pendaftaran gagal!";
      }
      ?>
    </p>
  <?php endif; ?>
</body>

</html>