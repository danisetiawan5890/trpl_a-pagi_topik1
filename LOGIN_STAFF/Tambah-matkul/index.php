<?php

include '../../config/config.php';

session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../../LOGIN_STAFF/index.php");
}

$kode_matkul          = "";
$id_user              = "";
$nama_matkul          = "";
$nama_dosen           = "";
$sukses               = "";
$error                = "";

if (isset($_GET['op'])) {
  $op = $_GET['op'];
} else {
  $op = "";
}

if ($op == 'delete') {
  $id_matkul    = $_GET['id_matkul'];
  $sql    = "DELETE FROM mata_kuliah where id_matkul = '$id_matkul'";
  $query  = mysqli_query($con, $sql);
  if ($query) {
    $sukses = "Berhasil hapus data";
  } else {
    $error  = "Gagal melakukan delete data";
  }
}

// edit mahasiswa
if ($op == 'edit') {
  $id_matkul                       = $_GET['id_matkul'];
  $sql                      = "SELECT * FROM mata_kuliah where id_matkul = '$id_matkul'";
  $query                    = mysqli_query($con, $sql);
  $r1                       = mysqli_fetch_array($query);
  $kode_matkul                 = $r1['kode_matkul'];
  $id_user                     = $r1['id_user'];
  $nama_matkul                 = $r1['nama_matkul'];
  $nama_dosen                  = $r1['nama_dosen'];
}

if (isset($_POST['simpan'])) {

  // ambil data dari formulir
  $kode_matkul                 = $_POST['kode_matkul'];
  $id_user                     = $_POST['id_user'];
  $nama_matkul                 = $_POST['nama_matkul'];
  $nama_dosen                  = $_POST['nama_dosen'];

  if ($kode_matkul && $id_user && $nama_matkul && $nama_dosen) {
    if ($op == 'edit') { //untuk update
      $sql    = "UPDATE mata_kuliah set kode_matkul = '$kode_matkul', id_user = '$id_user', nama_matkul = '$nama_matkul', nama_dosen = '$nama_dosen' where id_matkul = '$id_matkul'";
      $query  = mysqli_query($con, $sql);
      if ($query) {
        $sukses = "Data berhasil di update";
      } else {
        $error  = "Data gagal diupdate";
      }
    } else { // untuk daftar
      $sql = "INSERT INTO mata_kuliah (kode_matkul, id_user, nama_matkul, nama_dosen) VALUE ('$kode_matkul', '$id_user', '$nama_matkul', '$nama_dosen')";
      $query = mysqli_query($con, $sql);
    }
  } else {
    $error = "Silahkan masukkan semua data";
  }

  // apakah query simpan berhasil
  if ($query) {
    // kalau berhasil alihkan ke halaman index.php
    header('Location: ../index.php?status=sukses');
  } else {
    // kalau gagal alihkan ke halaman index.php dengan status=gagal
    header('Location: ../Tambah/index.php?status=gagal');
  }
}

?>


<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">

<head>
  <meta charset="UTF-8" />
  <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
  <link rel="stylesheet" href="style.css" />
  <!-- Boxicons CDN Link -->
  <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <style>
    /* css table */
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #081d45;
      color: white;
    }

    /* css table */


    /* button */
    /* CSS */
    .button-1 {
      background-color: #FFA500;
      border-radius: 8px;
      border-style: none;
      box-sizing: border-box;
      color: #FFFFFF;
      cursor: pointer;
      display: inline-block;
      font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      font-weight: 500;
      height: 40px;
      line-height: 20px;
      list-style: none;
      margin: 0;
      outline: none;
      padding: 10px 16px;
      position: relative;
      text-align: center;
      text-decoration: none;
      transition: color 100ms;
      vertical-align: baseline;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
    }

    .button-1:hover,
    .button-1:focus {
      background-color: #F082AC;
    }

    .button-2 {
      background-color: #8B0000;
      border-radius: 8px;
      border-style: none;
      box-sizing: border-box;
      color: #FFFFFF;
      cursor: pointer;
      display: inline-block;
      font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      font-weight: 500;
      height: 40px;
      line-height: 20px;
      list-style: none;
      margin: 0;
      outline: none;
      padding: 10px 16px;
      position: relative;
      text-align: center;
      text-decoration: none;
      transition: color 100ms;
      vertical-align: baseline;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
    }

    .button-1:hover,
    .button-1:focus {
      background-color: #F082AC;
    }

    /* button */
  </style>
</head>

<body>
  <div class="sidebar">
    <div class="logo-details">
      <i class="bx bx-notepad"></i>
      <span class="logo_name">Attendance Report</span>
    </div>
    <ul class="nav-links">
      <li>
        <a href="../index.php">
          <i class="bx bx-grid-alt"></i>
          <span class="links_name">Beranda</span>
        </a>
      </li>
      <li>
        <a href="../Tambah/index.php">
          <i class="bx bx-user"></i>
          <span class="links_name">Tambah Pengguna</span>
        </a>
      </li>
      <li>
        <a href="../Tambah-matkul/index.php" class="active">
        <i class="bx bx-food-menu"></i>
          <span class="links_name">Tambah Mata Kuliah</span>
        </a>
      </li>
      <li class="log_out">
        <a href="../../login/index.php">
          <i class="bx bx-log-out"></i>
          <span class="links_name">Keluar</span>
        </a>
      </li>
    </ul>
  </div>
  <section class="home-section">
    <nav>
      <div class="sidebar-button">
        <i class="bx bx-menu sidebarBtn"></i>
        <span class="dashboard">Tambah Mata Kuliah</span>
      </div>
    </nav>


    <div class="home-content">
      <?php
      if ($error) {
      ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $error ?>
        </div>
      <?php
        
      }
      ?>
      <?php
      if ($sukses) {
      ?>
        <div class="alert alert-success" role="alert">
          <?php echo $sukses ?>
        </div>
      <?php
        
      }
      ?>
      <div class="sales-boxes">
        <div class="recent-sales box">
          <div class="title">Mengelola Data</div>
          <div class="content">
            <form action="" method="POST">
              <div class="user-details">
                <div class="input-box">
                  <span class="details" for="kode_matkul">Kode Mata Kuliah</span>
                  <input type="text" name="kode_matkul" id="kode_matkul" placeholder="Masukkan Kode Matkul" class="read-only" value="<?php echo $kode_matkul ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="nama_dosen">Dosen</span>
                  <input type="text" name="nama_dosen" id="nama_dosen" placeholder="Nama Dosen" value="<?php echo $nama_dosen ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="id_user">ID Pengguna</span>
                  <input type="text" name="id_user" id="id_user" placeholder="Masukkan ID Pengguna" value="<?php echo $id_user ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="nama_matkul">Mata Kuliah</span>
                  <input type="text" name="nama_matkul" id="nama_matkul" placeholder="Nama Mata Kuliah" value="<?php echo $nama_matkul ?>" />
                </div>
                
              </div>
              <div class="button">
                <input type="submit" value="Simpan Data" name="simpan" />
              </div>
            </form>
            <!-- form mengeluarkan data -->
            <div class="title">Data Mata Kuliah</div>
            <div class="">
              <table id="customers">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Kode Mata Kuliah</th>
                    <th>ID User</th>
                    <th>Nama Mata Kuliah</th>
                    <th>Nama Dosen</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sql    = "SELECT * FROM mata_kuliah order by id_matkul desc";
                  $query  = mysqli_query($con, $sql);
                  $urut   = 1;
                  while ($mahasiswa = mysqli_fetch_array($query)) {
                    $id_matkul                       = $mahasiswa['id_matkul'];
                    $kode_matkul                 = $mahasiswa['kode_matkul'];
                    $id_user                     = $mahasiswa['id_user'];
                    $nama_matkul                    = $mahasiswa['nama_matkul'];
                    $nama_dosen                 = $mahasiswa['nama_dosen'];

                  ?>
                    <tr>
                      <td><?php echo $urut++ ?></td>
                      <td><?php echo $kode_matkul ?></td>
                      <td><?php echo $id_user ?></td>
                      <td><?php echo $nama_matkul ?></td>
                      <td><?php echo $nama_dosen ?></td>
                      <td scope="row">
                        <a href="index.php?op=edit&id_matkul=<?php echo $id_matkul ?>"><button type="button" class="button-1">Edit</button></a>
                        <a href="index.php?op=delete&id_matkul=<?php echo $id_matkul ?>" onclick="return confirm('Yakin mau delete data?')"><button type="button" class="button-2">Delete</button></a>
                      </td>
                    </tr>

                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    let sidebar = document.querySelector('.sidebar');
    let sidebarBtn = document.querySelector('.sidebarBtn');
    sidebarBtn.onclick = function() {
      sidebar.classList.toggle('active');
      if (sidebar.classList.contains('active')) {
        sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
      } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
    };
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>