<?php

include '../../config/config.php';

session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../../LOGIN_STAFF/index.php");
}

$username             = "";
$nama                 = "";
$prodi                = "";
$password             = "";
$sebagai              = "";
$jenis_kelamin        = "";
$sukses               = "";
$error                = "";

if (isset($_GET['op'])) {
  $op = $_GET['op'];
} else {
  $op = "";
}

if ($op == 'delete') {
  $id    = $_GET['id'];
  $sql    = "DELETE FROM tb_user where id = '$id'";
  $query  = mysqli_query($con, $sql);
  if ($query) {
    $sukses = "";
  } else {
    $error  = "Gagal melakukan delete data";
  }
}

// edit mahasiswa
if ($op == 'edit') {
  $id                       = $_GET['id'];
  $sql                      = "SELECT * FROM tb_user where id = '$id'";
  $query                    = mysqli_query($con, $sql);
  $r1                       = mysqli_fetch_array($query);
  $username                 = $r1['username'];
  $nama                     = $r1['nama'];
  $prodi                    = $r1['prodi'];
  $password                 = $r1['password'];
  $sebagai                  = $r1['sebagai'];
  // $jenis_kelamin  = $r1['gender'];


}

if (isset($_POST['simpan'])) {

  // ambil data dari formulir
  $username       = $_POST['username'];
  $nama           = $_POST['nama'];
  $prodi          = $_POST['prodi'];
  $password       = $_POST['password'];
  $sebagai        = $_POST['sebagai'];
  $jenis_kelamin  = $_POST['gender'];

  if ($username && $nama && $prodi && $password && $sebagai && $jenis_kelamin) {
    if ($op == 'edit') { //untuk update
      $sql    = "UPDATE tb_user set username = '$username', nama = '$nama', prodi = '$prodi', password = '$password', sebagai = '$sebagai', jenis_kelamin = '$jenis_kelamin' where id = '$id'";
      $query  = mysqli_query($con, $sql);
      if ($query) {
        $sukses = "Data berhasil di update";
      } else {
        $error  = "Data gagal diupdate";
      }
    } else { // untuk daftar
      $sql = "INSERT INTO tb_user (username, nama, prodi, password, sebagai, jenis_kelamin) VALUE ('$username', '$nama', '$prodi', '$password', '$sebagai', '$jenis_kelamin')";
      $query = mysqli_query($con, $sql);
    }
  } else {
    $error = "Silahkan masukkan semua data";
  }

  // apakah query simpan berhasil
  if ($query) {
    // kalau berhasil alihkan ke halaman index.php
    header('Location: ../index.php?status=sukses');
  } else {
    // kalau gagal alihkan ke halaman index.php dengan status=gagal
    header('Location: ../Tambah/index.php?status=gagal');
  }
}

?>


<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">

<head>
  <meta charset="UTF-8" />
  <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
  <link rel="stylesheet" href="style.css" />
  <!-- Boxicons CDN Link -->
  <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <style>
    /* css table */
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #081d45;
      color: white;
    }

    /* css table */


    /* button */
    /* CSS */
    .button-1 {
      background-color: #FFA500;
      border-radius: 8px;
      border-style: none;
      box-sizing: border-box;
      color: #FFFFFF;
      cursor: pointer;
      display: inline-block;
      font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      font-weight: 500;
      height: 40px;
      line-height: 20px;
      list-style: none;
      margin: 0;
      outline: none;
      padding: 10px 16px;
      position: relative;
      text-align: center;
      text-decoration: none;
      transition: color 100ms;
      vertical-align: baseline;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
    }

    .button-1:hover,
    .button-1:focus {
      background-color: #F082AC;
    }

    .button-2 {
      background-color: #8B0000;
      border-radius: 8px;
      border-style: none;
      box-sizing: border-box;
      color: #FFFFFF;
      cursor: pointer;
      display: inline-block;
      font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      font-weight: 500;
      height: 40px;
      line-height: 20px;
      list-style: none;
      margin: 0;
      outline: none;
      padding: 10px 16px;
      position: relative;
      text-align: center;
      text-decoration: none;
      transition: color 100ms;
      vertical-align: baseline;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
    }

    .button-1:hover,
    .button-1:focus {
      background-color: #F082AC;
    }

    /* button */
  </style>
</head>

<body>
  <div class="sidebar">
    <div class="logo-details">
      <i class="bx bx-notepad"></i>
      <span class="logo_name">Attendance Report</span>
    </div>
    <ul class="nav-links">
      <li>
        <a href="../index.php">
          <i class="bx bx-grid-alt"></i>
          <span class="links_name">Beranda</span>
        </a>
      </li>
      <li>
        <a href="../Tambah/index.php" class="active">
          <i class="bx bx-user"></i>
          <span class="links_name">Tambah Pengguna</span></span>
        </a>
      </li>
      <li>
        <a href="../Tambah-matkul/index.php">
        <i class="bx bx-food-menu"></i>
          <span class="links_name">Tambah Mata Kuliah</span>
        </a>
      </li>
      <li class="log_out">
        <a href="../../login/index.php">
          <i class="bx bx-log-out"></i>
          <span class="links_name">Keluar</span>
        </a>
      </li>
    </ul>
  </div>
  <section class="home-section">
    <nav>
      <div class="sidebar-button">
        <i class="bx bx-menu sidebarBtn"></i>
        <span class="dashboard">Tambah Pengguna</span>
      </div>
    </nav>


    <div class="home-content">
      <?php
      if ($error) {
      ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $error ?>
        </div>
      <?php
        
      }
      ?>
      <?php
      if ($sukses) {
      ?>
        <div class="alert alert-success" role="alert">
          <?php echo $sukses ?>
        </div>
      <?php
        
      }
      ?>
      <div class="sales-boxes">
        <div class="recent-sales box">
          <div class="title">Mengelola Data</div>
          <div class="content">
            <form action="" method="POST">
              <div class="user-details">
                <div class="input-box">
                  <span class="details" for="username">Pengguna</span>
                  <input type="text" name="username" id="username" placeholder="Masukkan Nama Pengguna" class="read-only" value="<?php echo $username ?>" />
                </div>
                <!-- <div class="input-box">
                    <span class="details">Username</span>
                    <input type="text" placeholder="Enter your username" required />
                  </div> -->
                <div class="input-box">
                  <span class="details" for="password">Kata Sandi</span>
                  <input type="password" name="password" id="password" placeholder="Masukkan Kata Sandi" value="<?php echo $password ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="nama">Nama</span>
                  <input type="text" name="nama" id="nama" placeholder="Masukkan Nama Lengkap" value="<?php echo $nama ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="sebagai">Sebagai</span>
                  <input type="text" name="sebagai" id="sebagai" placeholder="Masukkan Jabatan" value="<?php echo $sebagai ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="prodi">Program Studi / Mata Kuliah</span>
                  <input type="text" name="prodi" id="prodi" placeholder="Konfirmasi program studi / Mata Kuliah" value="<?php echo $prodi ?>" />
                </div>
              </div>
              <div class="gender-details">
                <input type="radio" name="gender" value="male" id="dot-1" />
                <input type="radio" name="gender" value="female" id="dot-2" />
                <span class="gender-title">Jenis Kelamin</span>
                <!-- <label for="jenis_kelamin" class="gender-title">Gender</label> -->
                <div class="category" for="jenis_kelamin">
                  <label for="dot-1">
                    <span class="dot one"></span>
                    <span class="gender">Laki-Laki</span>
                  </label>
                  <label for="dot-2">
                    <span class="dot two"></span>
                    <span class="gender">Perempuan</span>
                  </label>
                </div>
              </div>
              <div class="button">
                <input type="submit" value="Simpan Data" name="simpan" />
              </div>
            </form>

            <!-- search data (username/nim)-->
            <br>
                <div>
                  <form method="get" action="">
                    &emsp;&nbsp;<label for="cari" style="font-size: 20px; font-family:'Poppins', sans-serif; ">Cari data</label><br>
                    <i class="fas fa-search"></i>
                    <input type="text" name="cari" style="width: 300px; height: 40px; outline-style:outset; outline-color:purple; outline-width:thin; border-radius:5px;" placeholder="&nbsp Masukkan username. . ." autocomplete="off">
                    <a href="../Tambah/index.php" class="active" style="float: right;">
                      <i class="bx bx-user"></i>
                      <span class="links_name">Tampilkan semua data</span>
                    </a>
                  </form>
                  <br>

            <!-- form mengeluarkan data -->
            <div class="title">Data Pengguna</div>
            <div class="">
              <table id="customers">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Program Study</th>
                    <th>Password</th>
                    <th>Sebagai</th>
                    <th>Gender</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sql    = "SELECT * FROM tb_user order by id desc";
                  $query  = mysqli_query($con, $sql);
                  $urut   = 1;

                   //search data (username/nim)//
                   if (isset($_GET['cari'])) {
                    $query  = mysqli_query($con, "SELECT * FROM tb_user WHERE username LIKE '%" .
                      $_GET['cari'] . "%'");
                  }

                  while ($mahasiswa = mysqli_fetch_array($query)) {
                    $id                       = $mahasiswa['id'];
                    $username                 = $mahasiswa['username'];
                    $nama                     = $mahasiswa['nama'];
                    $prodi                    = $mahasiswa['prodi'];
                    $password                 = $mahasiswa['password'];
                    $sebagai                  = $mahasiswa['sebagai'];
                    $jenis_kelamin            = $mahasiswa['jenis_kelamin'];
                  ?>
                    <tr>
                      <td><?php echo $urut++ ?></td>
                      <td><?php echo $username ?></td>
                      <td><?php echo $nama ?></td>
                      <td><?php echo $prodi ?></td>
                      <td><?php echo $password ?></td>
                      <td><?php echo $sebagai ?></td>
                      <td><?php echo $jenis_kelamin ?></td>
                      <td scope="row">
                        <a href="index.php?op=edit&id=<?php echo $id ?>"><button type="button" class="button-1">Edit</button></a>
                        <a href="index.php?op=delete&id=<?php echo $id ?>" onclick="return confirm('Yakin mau delete data?')"><button type="button" class="button-2">Delete</button></a>
                      </td>
                    </tr>

                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    let sidebar = document.querySelector('.sidebar');
    let sidebarBtn = document.querySelector('.sidebarBtn');
    sidebarBtn.onclick = function() {
      sidebar.classList.toggle('active');
      if (sidebar.classList.contains('active')) {
        sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
      } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
    };
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>