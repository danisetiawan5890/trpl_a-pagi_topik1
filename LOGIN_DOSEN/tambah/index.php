<?php

include '../../config/config.php';

session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../../LOGIN_DOSEN/index.php");
}

$matkul                       = "";
$nama_matkul                  = "";
$nim                          = "";
$keterangan                   = "";
$sukses                       = "";
$error                        = "";

if (isset($_GET['op'])) {
  $op = $_GET['op'];
} else {
  $op = "";
}

if ($op == 'delete') {
  $id_absen     = $_GET['id_absen'];
  $sql          = "DELETE FROM tb_absensi where id_absen = '$id_absen'";
  $query        = mysqli_query($con, $sql);
  if ($query) {
    $sukses = "Berhasil hapus data";
  } else {
    $error  = "Gagal melakukan delete data";
  }
}

// edit mahasiswa
if ($op == 'edit') {
  $id_absen                 = $_GET['id_absen'];
  $sql                      = "SELECT * FROM tb_absensi where id_absen = '$id_absen'";
  $query                    = mysqli_query($con, $sql);
  $r1                       = mysqli_fetch_array($query);
  $matkul                   = $r1['matkul'];
  $nama_matkul              = $r1['nama_matkul'];
  $nim                      = $r1['nim'];
  $keterangan               = $r1['keterangan'];
  // $jenis_kelamin  = $r1['gender'];


}

if (isset($_POST['simpan'])) {

  // ambil data dari formulir
  $matkul                   = $_POST['matkul'];
  $nama_matkul              = $_POST['nama_matkul'];
  $nim                      = $_POST['nim'];
  $keterangan               = $_POST['keterangan'];

  if ( $keterangan) {
    if ($op == 'edit') { //untuk update
      $sql    = "UPDATE tb_absensi set keterangan = '$keterangan' where id_absen = '$id_absen'";
      $query  = mysqli_query($con, $sql);
      if ($query) {
        $sukses = "Data berhasil di update";
      } else {
        $error  = "Data gagal diupdate";
      }
    }
  } else {
    $error = "Silahkan masukkan semua data";
  }

  // apakah query simpan berhasil
  if ($query) {
    // kalau berhasil alihkan ke halaman index.php
    header('Location: ../index.php?status=sukses');
  } else {
    // kalau gagal alihkan ke halaman index.php dengan status=gagal
    header('Location: ../tambah/index.php?status=gagal');
  }
}

?>


<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">

<head>
  <meta charset="UTF-8" />
  <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
  <link rel="stylesheet" href="style.css" />
  <!-- Boxicons CDN Link -->
  <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <style>
    /* css table */
    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    #customers td,
    #customers th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    #customers tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    #customers tr:hover {
      background-color: #ddd;
    }

    #customers th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #081d45;
      color: white;
    }

    /* css table */


    /* button */
    /* CSS */
    .button-1 {
      background-color: #FFA500;
      border-radius: 8px;
      border-style: none;
      box-sizing: border-box;
      color: #FFFFFF;
      cursor: pointer;
      display: inline-block;
      font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      font-weight: 500;
      height: 40px;
      line-height: 20px;
      list-style: none;
      margin: 0;
      outline: none;
      padding: 10px 16px;
      position: relative;
      text-align: center;
      text-decoration: none;
      transition: color 100ms;
      vertical-align: baseline;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
    }

    .button-1:hover,
    .button-1:focus {
      background-color: #F082AC;
    }

    .button-2 {
      background-color: #8B0000;
      border-radius: 8px;
      border-style: none;
      box-sizing: border-box;
      color: #FFFFFF;
      cursor: pointer;
      display: inline-block;
      font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      font-weight: 500;
      height: 40px;
      line-height: 20px;
      list-style: none;
      margin: 0;
      outline: none;
      padding: 10px 16px;
      position: relative;
      text-align: center;
      text-decoration: none;
      transition: color 100ms;
      vertical-align: baseline;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
    }

    .button-1:hover,
    .button-1:focus {
      background-color: #F082AC;
    }

    /* button */
  </style>
</head>

<body>
  <div class="sidebar">
    <div class="logo-details">
      <i class="bx bx-notepad"></i>
      <span class="logo_name">Attendance Report</span>
    </div>
    <ul class="nav-links">
      <li>
        <a href="../index.php">
          <i class="bx bx-grid-alt"></i>
          <span class="links_name">Beranda</span>
        </a>
      </li>
      <li>
        <a href="../Tambah/index.php" class="active">
          <i class="bx bx-user"></i>
          <span class="links_name">Rekap Kehadiran</span>
        </a>
      </li>
      <li class="log_out">
        <a href="../../login/index.php">
          <i class="bx bx-log-out"></i>
          <span class="links_name">Keluar</span>
        </a>
      </li>
    </ul>
  </div>
  <section class="home-section">
    <nav>
      <div class="sidebar-button">
        <i class="bx bx-menu sidebarBtn"></i>
        <span class="dashboard"> Ubah Kehadiran</span>
      </div>
    </nav>


    <div class="home-content">
      <?php
      if ($error) {
      ?>
        <div class="alert alert-danger" role="alert">
          <?php echo $error ?>
        </div>
      <?php
        header("refresh:2;url=index.php"); //2 : detik
      }
      ?>
      <?php
      if ($sukses) {
      ?>
        <div class="alert alert-success" role="alert">
          <?php echo $sukses ?>
        </div>
      <?php
        header("refresh:2;url=index.php");
      }
      ?>
      <div class="sales-boxes">
        <div class="recent-sales box">
          <div class="title">Ubah</div>
          <div class="content">
            <form action="" method="POST">
              <div class="user-details">
                <div class="input-box">
                  <span class="details" for="matkul">Kode Matkul</span>
                  <input type="text" name="matkul" id="matkul" disabled class="read-only" value="<?php echo $matkul ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="nama_matkul">Mata Kuliah</span>
                  <input type="text" name="nama_matkul" id="nama_matkul" disabled value="<?php echo $nama_matkul ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="nim">NIM</span>
                  <input type="text" name="nim" id="nim" disabled value="<?php echo $nim ?>" />
                </div>
                <div class="input-box">
                  <span class="details" for="keterangan">Keterangan</span>
                  <input type="text" name="keterangan" id="keterangan"  value="<?php echo $keterangan ?>" />
                </div>
              </div>
              <div class="button">
                <input type="submit" value="Simpan Data" name="simpan" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script>
    let sidebar = document.querySelector('.sidebar');
    let sidebarBtn = document.querySelector('.sidebarBtn');
    sidebarBtn.onclick = function() {
      sidebar.classList.toggle('active');
      if (sidebar.classList.contains('active')) {
        sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
      } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
    };
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>