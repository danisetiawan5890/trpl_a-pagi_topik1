<?php
include '../../config/config.php';

date_default_timezone_set('asia/jakarta');
session_start();

if (!isset($_SESSION['sebagai'])) {
  header("Location: ../LOGIN_DOSEN/index.php");
}

?>
<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8" />
    <!--<title> Responsiive Admin Dashboard | CodingLab </title>-->
    <link rel="stylesheet" href="style.css" />
    <!-- Boxicons CDN Link -->
    <link href="https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- datepicker nya -->
     <!-- Custom styles for this template-->
    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../vendor/datepicker/datepicker3.css">

  </head>
  <body>
    <div class="sidebar">
      <div class="logo-details">
        <i class="bx bx-notepad"></i>
        <span class="logo_name">Attendance Report</span>
      </div>
      <ul class="nav-links">
        <li>
          <a href="../index.php">
            <i class="bx bx-grid-alt"></i>
            <span class="links_name">Beranda</span>
          </a>
        </li>

        <li>
          <a href="#" class="active">
            <i class="bx bx-food-menu"></i>
            <span class="links_name">Rekap Kehadiran</span>
          </a>
        </li>
        <li class="log_out">
          <a href="../../login/index.php">
            <i class="bx bx-log-out"></i>
            <span class="links_name">Keluar</span>
          </a>
        </li>
      </ul>
    </div>
    <section class="home-section">
      <nav>
        <div class="sidebar-button">
          <i class="bx bx-menu sidebarBtn"></i>
          <span class="dashboard">Rekap Kehadiran</span>
        </div>
      </nav>

      <div class="home-content">
        <div class="sales-boxes">
          <div class="recent-sales box">
            <div class="activity">
             
            <!-- new code -->
            <div class="card-header">
            <form action="" method="post">
                <div class="form-group form-row">
                    <div class="col">
                        <input type="date" class="form-control" name="tgl1" required>
                    </div>
                    <div class="col">
                        <div class="input-group">
                            <input type="date" class="form-control" name="tgl2" required>
                            <div class="input-group-append">
                                <button type="submit" name="proses" class="btn btn-primary">Proses</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        <?php
        if(isset($_POST['proses'])){
            $tgl1 = $_POST['tgl1'];
            $tgl2 = $_POST['tgl2'];
        ?>
        <div class="card-body">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <td colspan="9" align="center">
                            <h5 class="m-0 font-weight-bold text-primary text-center">
                                Laporan Absensi Mahasiswa Dari <?php echo $tgl1 ?> s/d <?php echo $tgl2 ?>
                            </h5>
                        </td>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Kode matkul</th>
                        <th>Mata Kuliah</th>
                        <th>NIM</th>
                        <th>Masuk</th>
                        <th>Terlambat</th>
                        <th>Tanggal</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $f = $_SESSION['prodi'];
                        $query = mysqli_query($con, "SELECT * FROM tb_absensi WHERE nama_matkul = '$f'");
                        $no = 1;
                        while ($data = mysqli_fetch_array($query)) {
                          $id_absen                 = $data['id_absen'];
                          $matkul                   = $data['matkul'];
                          $nama_matkul              = $data['nama_matkul'];
                          $nim                      = $data['nim'];
                          $keterangan               = $data['keterangan'];
                        ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data['matkul'] ?></td>
                        <td><?= $data['nama_matkul'] ?></td>
                        <td><?= $data['nim'] ?></td>
                        <td><?= $data['masuk'] ?></td>
                        <td><?= $data['terlambat'] ?></td>
                        <td><?= $data['tgl_absen'] ?></td>
                        <td><?= $data['keterangan'] ?></td>
                        <td>
                          <a href="../tambah/index.php?op=edit&id_absen=<?php echo $id_absen  ?>"><button type="button" class="btn btn-primary">Ubah</button></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php } ?>
    </div>




      <!-- tutup container -->
        </div>
            
            </div>
          </div>
    </section>

    <script>
      let sidebar = document.querySelector('.sidebar');
      let sidebarBtn = document.querySelector('.sidebarBtn');
      sidebarBtn.onclick = function () {
        sidebar.classList.toggle('active');
        if (sidebar.classList.contains('active')) {
          sidebarBtn.classList.replace('bx-menu', 'bx-menu-alt-right');
        } else sidebarBtn.classList.replace('bx-menu-alt-right', 'bx-menu');
      };
    </script>
  </body>
</html>
